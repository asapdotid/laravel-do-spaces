<?php

namespace App\Http\Controllers;

use App\Models\CmsUsers;
use DB;

class TestController extends Controller
{
    public function index()
    {
        $results = [];
        // if (DB::connection()->getDatabaseName()) {
        //     echo 'conncted sucessfully to database '.DB::connection()->getDatabaseName();
        // }

        $users = CmsUsers::all()->orderBy('id', 'DESC');
        $i = 0;

        foreach ($users as $user) {
            $results[$i] = $user->email;
            ++$i;
        }

        return $results;
    }
}
